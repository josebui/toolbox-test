import supertest from 'supertest'
import chai from 'chai'

const { assert } = chai

const server = supertest.agent('http://localhost:5005')

describe('API tests',function(){

  it('Should return same message',function(done){
    const message = 'test message'
    server
      .post('/api/process')
      .send({ message })
      .expect("Content-type",/json/)
      .expect(200)
      .end((err,res) => {
        assert.equal(res.status, 200, 'Should accept request')
        assert.equal(res.body.message, message, 'Message should be the same')
        done()
      })
  })
})
