
export const process = (req, res, next) => {
  const { message } = req.body
  res.json({ message })
}
