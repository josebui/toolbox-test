import express from 'express'

import * as controller from '~/api.controller'

const router = express.Router()
router.post('/process', controller.process)

export default router
