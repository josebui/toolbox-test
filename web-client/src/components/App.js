import React from 'react'

import { Navbar } from 'react-bootstrap'

import ProcessForm from '../process/components/ProcessForm'

const App = () => (
  <div>
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          Toolbox - Test
        </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
    <ProcessForm />
  </div>
)

export default App
