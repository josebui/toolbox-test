import { handleActions } from 'redux-actions'

import * as actions from './process.actions'


const initialState = {
  message: null,
  isProcessing: false,
  error: null,
}

export default handleActions({
  [actions.processRequest]: state => (
    { ...state, isProcessing: true, error: null }
  ),
  [actions.processSuccess]: (state, action) => ({
    ...state,
    message: action.payload.message,
    isProcessing: false,
    error: null,
  }),
  [actions.processError]: state => (
    { ...state, error: 'Failed to process message', isProcessing: false }
  ),
}, initialState)
