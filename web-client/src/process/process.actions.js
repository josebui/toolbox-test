import { createAction } from 'redux-actions'

import * as service from './process.service'


export const processRequest = createAction('PROCESS_REQUEST')
export const processSuccess = createAction('PROCESS_SUCCESS')
export const processError = createAction('PROCESS_ERROR')

export const process = message => dispatch => {
  dispatch(processRequest())
  return service.process(message)
    .then(receivedMessage => dispatch(processSuccess(receivedMessage)))
    .catch(error => dispatch(processError(error)))
}
