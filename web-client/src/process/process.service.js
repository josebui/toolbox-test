
export const process = message => fetch('http://localhost:5005/api/process', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    message,
  }),
})
  .then(res => {
    if (res.status !== 200) {
      return Promise.reject(res)
    }
    return res.json()
  })
