/* global jest */
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'

import * as actions from './process.actions'


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Process actions', () => {
  it('Should dispatch actions when process()', () => {
    const expectedActions = [
      actions.processRequest(),
      actions.processSuccess(),
    ]
    const store = mockStore({})

    window.fetch = jest.fn().mockImplementation(() => Promise.resolve({
      status: 200,
      json: () => ({ message: 'test' }),
    }))

    return store.dispatch(actions.process())
      .then(() => {
        expect(
          store.getActions().map(action => action.type)
        ).toEqual(
          expectedActions.map(action => action.type)
        )
      })
  })
})
