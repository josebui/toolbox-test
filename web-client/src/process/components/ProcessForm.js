import React from 'react'
import { connect } from 'react-redux'

import {
  FormControl,
  ControlLabel,
  FormGroup,
  Button,
  Panel,
  Alert,
} from 'react-bootstrap'

import * as actions from '../process.actions'


class ProcessForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      message: '',
    }
  }

  onMessageChange(event) {
    this.setState({
      message: event.target.value,
    })
  }

  onSubmit() {
    this.props.process(this.state.message)
  }

  showError() {
    const { error } = this.props
    if (!error) {
      return null
    }
    return (
      <Alert bsStyle="danger">
        <p>{error}</p>
      </Alert>
    )
  }

  showMessage() {
    const { message } = this.props
    if (!message) {
      return null
    }
    return (
      <Alert bsStyle="success">
        <h4>Received Message:</h4>
        <p>{message}</p>
      </Alert>
    )
  }

  render() {
    return (
      <Panel>
        <Panel.Body>
          {this.showError()}
          {this.showMessage()}
          <FormGroup
            controlId="formBasicText"
          >
            <ControlLabel>Message</ControlLabel>
            <FormControl
              type="text"
              value={this.state.message}
              placeholder="Enter message"
              onChange={this.onMessageChange.bind(this)}
            />
          </FormGroup>
          <Button type="submit" onClick={this.onSubmit.bind(this)}>Process message</Button>
        </Panel.Body>
      </Panel>
    )
  }
}

export default connect(state => ({
  message: state.process.message,
  error: state.process.error,
}), dispatch => ({
  process: message => dispatch(actions.process(message)),
}))(ProcessForm)
