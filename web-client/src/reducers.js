import { combineReducers } from 'redux'

import process from './process/process.reducer'


export default combineReducers({
  process,
})
