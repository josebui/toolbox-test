Toolbox Test
=========


# Quick start

Go to the `dist/` folder and open the `index.html` file on any modern browser

# Development requirements
- Docker

# Development

```
npn run build
npm run start
```

# Production

```
npm run dist
```
After that you can just go to the dist folder and open the index.html file


# Test

```
npm run test
```
Or
```
npm run test:watch
```

# Lint
```
npm run lint
```
