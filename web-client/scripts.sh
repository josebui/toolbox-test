#!/bin/bash

DOCKER_IMAGE="toolbox-web-image"
DOCKER_CONTAINER="toolbox-web"

function build() {
  docker build \
    -f Dockerfile \
    -t "$DOCKER_IMAGE" .
}

function runContainer() {
  docker run -it --rm --name "$DOCKER_CONTAINER" \
    -v "$(pwd)":/app \
    -p "8080:8080" "$DOCKER_IMAGE:latest" \
    bash -c "npm install && $1"
}

function runCommand() {
  container_exist="$(docker ps -a -q -f name=$DOCKER_CONTAINER)"
  if [ ! $container_exist ]; then
    # Container doesnt exist, run container and command
    runContainer "$1"
  else
    # Container running, run command inside container
    docker exec -it "$DOCKER_CONTAINER" bash -c "$1"
  fi
}

function serve() {
  runCommand "node_modules/.bin/webpack-dev-server --mode development --host 0.0.0.0"
}

function dist() {
  runCommand "node_modules/.bin/webpack --mode production"
}

function test() {
  runCommand "node_modules/.bin/jest"
}

function test:watch() {
  runCommand "node_modules/.bin/jest --watch"
}

function lint() {
  runCommand "node_modules/.bin/eslint ./src"
}

if [ -z "$1" ]; then
  echo "Function name is required";
fi

# Execute function
$1
